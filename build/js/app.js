
"use strict"
document.getElementsByClassName('button__ok')[0].addEventListener("click", doNormal)
function doNormal(){
	
	var targetedText = document.forms.js__formA.elements.js__textA.value;
	var editedText;
	if(document.forms.js__formA.elements.js__textC.value){
			addText();} //проверяем, есть ли доп. слова
		
	if(document.forms.js__formA.elements.needTargetWords.checked){ //а нужно ли вообще править переносы
			replacePretext();} else editedText = targetedText;
	
	choiceStyleText();
	
	function replacePretext(){	// функция редактирования переносов
		var pretexts = ['и', 'о', 'не', 'а', 'при', 'или', 'что', 'это', 'бы', 'также', 'из-за', 'на', 'из', 'к', 'над', 'у', 'в', 'за', 'под', 'из-под', 'до', 'вокруг', 'мимо', 'между', 'около', 'перед', 'через', 'поперёк', 'среди', 'против', 'подле', 'возле', 'близ', 'вдоль', 'вне', 'внутри', 'сквозь', 'благодаря', 'по', 'за', 'от', 'из-за', 'ввиду', 'вследствие', 'для', 'ради', 'по', 'с', 'в течение', 'без'];

		var pretextsUpperCase = pretexts.map(function(name){
			return name.charAt(0).toUpperCase() + name.slice(1);
		});
		// добавляем предлоги с большой буквы
		var count = pretexts.length;
		for(var j=0;j<count;j++){
			pretexts.push(pretextsUpperCase[j]);
		}
		//превращаем в регулярные выражения
		var pretextsRegs = pretexts.map(function(name){
		return new RegExp(' ' + name + ' ','g');
		});
		//отредактированные предлоги
		var pretextsEdited = pretexts.map(function(name){
			return " " + name + "&nbsp;";
		});
		//заменяем предлоги
		for (var i=0; i<pretextsRegs.length; i++){

		editedText = targetedText.replace( pretextsRegs[i], pretextsEdited[i] );
		targetedText = editedText;
		
		}

		
	}
		
	function addText(){ // функция добавления слов в nobr
		var addWordsString = document.forms.js__formA.elements.js__textC.value;
		var addWordsArray = addWordsString.split(', ');
			
		//превращаем в регулярные выражения
		var addWordsArrayRegs = addWordsArray.map(function(name){
		return new RegExp(name,'g');
		});
		//отредактированные доп слова
		var addWordsArrayEdited = addWordsArray.map(function(name){
			return "<nobr>" + name + "</nobr>";
		});
		
		//заменяем слова
		for (var k=0; k<addWordsArrayRegs.length; k++){

		editedText = targetedText.replace( addWordsArrayRegs[k], addWordsArrayEdited[k] );
		targetedText = editedText;
		}
	}

	function choiceStyleText(){ //функция выбора стиля текста
		switch(true) {
			case document.forms.js__formA.elements.radio[0].checked:
			break;

			case document.forms.js__formA.elements.radio[1].checked:
			editedText = targetedText.toUpperCase();
			break;

			case document.forms.js__formA.elements.radio[2].checked:
			editedText = targetedText.toLowerCase();
			break;


			default:
			console.log("ошибка в выборе стиля текста")
		}
	}


	document.forms.js__formA.elements.js__textB.value = editedText;
	document.getElementsByClassName('counter')[0].innerHTML = editedText.length; //добавляем подсчет символов

	
	
}



