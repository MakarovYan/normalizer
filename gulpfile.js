"use strict";

const gulp   	  = require('gulp'),
	  sass   	  = require('gulp-sass'),
	  rename 	  = require('gulp-rename'),
	  uglify 	  = require('gulp-uglifyjs'),
	  del  		  = require('del'),
	  autoprefixer= require('gulp-autoprefixer'),
	  imagemin 	  = require('gulp-imagemin'),
	  browserSync = require('browser-sync');

gulp.task('sass', function(){
	return gulp.src('dev/sсss/**/*.sсss')
	.pipe(sass())
	.pipe(autoprefixer(['last 15 versions', '>1%', 'ie 8'], {cascade: true}))
	.pipe(rename({basename:'style'}))
	.pipe(gulp.dest('dev/css'))
	.pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts', function(){
	return gulp.src ('dev/js/app.js')
	.pipe(uglify())
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dev/js'));
});



gulp.task('browser-sync', function(){
	browserSync.init({
		server: {
			baseDir : 'dev'
		},
		notify: false //отключаем уведомления
	});
});

gulp.task('clean', function(){
	return del.sync('build');
})

gulp.task('watch', ['browser-sync', 'scripts'], function(){
	gulp.watch('dev/sсss/**/*.sсss' , ['sass']);
	gulp.watch('dev/**/*.html', browserSync.reload);
	gulp.watch('dev/**/*.js', browserSync.reload);
});


gulp.task('clean', function(){
	return del.sync('build');
});

gulp.task('img', function(){
	return gulp.src('dev/img/**/*.{png,jpg,jpeg,ico}')
	.pipe(imagemin({
	    interlaced: true,
	    progressive: true,
	    optimizationLevel: 5,
	    svgoPlugins: [{removeViewBox: true}]
	}))
	.pipe(gulp.dest('build/img'))
});

gulp.task('build', ['clean', 'sass', 'scripts', 'img'], function(){

	//css
	var buildCss = gulp.src('dev/css/**/*.css')
		.pipe(gulp.dest('build/css'));
	
	//js
	var buildJs = gulp.src('dev/js/**/*.js')
		.pipe(gulp.dest('build/js'));
	
	//html
	var buildHtml = gulp.src('dev/*.html')
		.pipe(gulp.dest('build'));


});
